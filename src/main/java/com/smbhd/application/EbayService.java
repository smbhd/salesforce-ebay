package com.smbhd.application;

import com.ebay.sdk.ApiContext;
import com.ebay.sdk.ApiCredential;
import com.ebay.sdk.CallRetry;
import com.ebay.sdk.TimeFilter;
import com.ebay.sdk.call.*;
import com.ebay.sdk.pictureservice.eps.eBayPictureServiceXMLCall;
import com.ebay.soap.eBLBaseComponents.*;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.bind.XmlObject;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class EbayService {

    private ApiContext ebay;
    private String userPaypalAddress;

    @Value("${salesforce.siteurl}")
    private String siteUrl;

    @Autowired
    public EbayService(@Value("${ebay.user.token}") String ebayUserToken, @Value("${ebay.wseps.url}") String ebayWSEPSURL,
                       @Value("${ebay.wsapi.url}") String ebayWSAPIURL) throws Exception {
        ebay = new ApiContext();
        ebay.setApiServerUrl(ebayWSAPIURL);
        ebay.setEpsServerUrl(ebayWSEPSURL);
        ApiCredential ebayUserCredentials = ebay.getApiCredential();
        ebayUserCredentials.seteBayToken(ebayUserToken);

        GetUserPreferencesCall userPreferencesCall = new GetUserPreferencesCall(ebay);
        userPreferencesCall.setShowSellerPaymentPreferences(true);
        userPreferencesCall.getUserPreferences();
        userPaypalAddress = userPreferencesCall.getReturnedSellerPaymentPreferences().getDefaultPayPalEmailAddress();
    }

    /**
     * Creates an item that is ready to list on ebay. Populates specific attributes based on the recordType.
     * @param sfItem The Salesforce item to be converted
     * @param recordTypeDevName The recordType name
     * @return An ebay-ready item.
     * @throws IOException
     */
    public ItemType buildEbayItem(SObject sfItem, String recordTypeDevName) throws IOException, ParseException {
        ItemType ebayItem = new ItemType();
        ebayItem.setSite(SiteCodeType.US);
        ebayItem.setCurrency(CurrencyCodeType.USD);
        ebayItem.setCountry(CountryCodeType.US);
        ebayItem.setQuantity(1);
        ebayItem.setDispatchTimeMax(5);
        ebayItem.setLocation("US");
        ebayItem.setHitCounter(HitCounterCodeType.BASIC_STYLE);
        ebayItem.setPayPalEmailAddress(userPaypalAddress);

        return setItemAttributes(sfItem, ebayItem, recordTypeDevName);
    }

    /**
     * Attaches all information from a saleforce item into an ebay item.
     * @param sfItem The Salesforce item to be converted.
     * @param ebayItem The Ebay item to be updated.
     * @param recordTypeDevName The recordType name.
     * @return An ebay-ready item.
     * @throws IOException
     */
    public ItemType setItemAttributes(SObject sfItem, ItemType ebayItem, String recordTypeDevName) throws IOException, ParseException {
        ebayItem.setTitle((String)sfItem.getField("Item_Title__c"));

        // Item categories
        CategoryType primaryCategory = new CategoryType();
        primaryCategory.setCategoryID(sfItem.getField("ebay_Category_Id__c").toString());
        ebayItem.setPrimaryCategory(primaryCategory);

        // Store categories
        StorefrontType storefrontType = new StorefrontType();
        if (sfItem.getField("Main_Store_Category__c") != null) {
            storefrontType.setStoreCategoryName((String)sfItem.getField("Main_Store_Category__c"));
            if (sfItem.getField("Designer__c") != null) {
                storefrontType.setStoreCategory2Name((String)sfItem.getField("Secondary_Store_Category__c"));
            }
        }
        ebayItem.setStorefront(storefrontType);

        // Item status
        if ("New with tags".equals(sfItem.getField("Condition__c").toString())) {
            ebayItem.setConditionID(1000);
        } else if ("New without tags".equals(sfItem.getField("Condition__c").toString())) {
            ebayItem.setConditionID(1500);
        } else if ("New with defects".equals(sfItem.getField("Condition__c").toString())) {
            ebayItem.setConditionID(1750);
        } else { // Pre-owned
            ebayItem.setConditionID(3000);
        }

        if (sfItem.getField("Condition_Detail__c") != null) {
            ebayItem.setConditionDescription(sfItem.getField("Condition_Detail__c").toString());
        }

        if (sfItem.getField("Retail_Price__c") != null) {
            DiscountPriceInfoType discountPriceInfoType = new DiscountPriceInfoType();
            AmountType originalRetailPrice = new AmountType();
            originalRetailPrice.setValue(Double.valueOf(sfItem.getField("Retail_Price__c").toString()));
            discountPriceInfoType.setOriginalRetailPrice(originalRetailPrice);
            ebayItem.setDiscountPriceInfo(discountPriceInfoType);
        }

        // Auction and Buy it Now
        if ("Auction".equals(sfItem.getField("Ebay_Listing_Type__c").toString())) {
            ebayItem.setListingType(ListingTypeCodeType.CHINESE); // CHINESE auction is an auction with an item having quantity 1
            AmountType amount = new AmountType();
            amount.setValue(Double.valueOf(sfItem.getField("Starting_Price__c").toString()));
            ebayItem.setStartPrice(amount);
        } else if ("Buy it Now".equals(sfItem.getField("Ebay_Listing_Type__c").toString())) {
            if (sfItem.getField("Accept_Best_Offer__c") != null && Boolean.valueOf(sfItem.getField("Accept_Best_Offer__c").toString())) {
                BestOfferDetailsType offerDetailsType = new BestOfferDetailsType();
                offerDetailsType.setBestOfferEnabled(true);
                ebayItem.setBestOfferDetails(offerDetailsType);

                // Auto Accept/Deny
                if (sfItem.getField("Auto_Accept_Over__c") != null || sfItem.getField("Auto_Deny_Under__c") != null) {
                    ListingDetailsType listingdetails = new ListingDetailsType();
                    if (sfItem.getField("Auto_Accept_Over__c") != null) {
                        AmountType bestOfferAutoAccept = new AmountType();
                        bestOfferAutoAccept.setValue(Double.valueOf(sfItem.getField("Auto_Accept_Over__c").toString()));
                        listingdetails.setBestOfferAutoAcceptPrice(bestOfferAutoAccept);
                    }
                    if (sfItem.getField("Auto_Deny_Under__c") != null) {
                        AmountType minimumBestOfferPrice = new AmountType();
                        minimumBestOfferPrice.setValue(Double.valueOf(sfItem.getField("Auto_Deny_Under__c").toString()));
                        listingdetails.setMinimumBestOfferPrice(minimumBestOfferPrice);
                    }
                    ebayItem.setListingDetails(listingdetails);
                }
            }
            ebayItem.setListingType(ListingTypeCodeType.FIXED_PRICE_ITEM);
            AmountType amount = new AmountType();
            amount.setValue(Double.valueOf(sfItem.getField("Buy_it_Now__c").toString()));
            ebayItem.setStartPrice(amount); // Start price in buy it now style listing IS the buy it now price
            if(!Boolean.valueOf(sfItem.getField("Immediate_Payment_Required__c").toString())) {
                ebayItem.setAutoPay(false);
            } else {
                ebayItem.setAutoPay(true);
            }
        }

        // Listing Duration
        if ("3 Days".equals(sfItem.getField("Listing_Duration__c").toString())) {
            ebayItem.setListingDuration(ListingDurationCodeType.DAYS_3.value());
        } else if ("5 Days".equals(sfItem.getField("Listing_Duration__c").toString())) {
            ebayItem.setListingDuration(ListingDurationCodeType.DAYS_5.value());
        } else if ("7 Days".equals(sfItem.getField("Listing_Duration__c").toString())) {
            ebayItem.setListingDuration(ListingDurationCodeType.DAYS_7.value());
        } else if ("10 Days".equals(sfItem.getField("Listing_Duration__c").toString())) {
            ebayItem.setListingDuration(ListingDurationCodeType.DAYS_10.value());
        } else if ("30 Days".equals(sfItem.getField("Listing_Duration__c").toString())) {
            ebayItem.setListingDuration(ListingDurationCodeType.DAYS_30.value());
        } else if ("Good Until Canceled".equals(sfItem.getField("Listing_Duration__c").toString())) {
            ebayItem.setListingDuration(ListingDurationCodeType.GTC.value());
        }

        // Scheduling listing
        if (sfItem.getField("Start_Time__c") != null) {
            Calendar now = Calendar.getInstance();
            // Make sure schedule time is in the future, otherwise just continue listing.
            Calendar startTime = Calendar.getInstance();
            SimpleDateFormat sfdcDateTimeParser = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
            sfdcDateTimeParser.setTimeZone(TimeZone.getTimeZone("GMT"));

            startTime.setTime(sfdcDateTimeParser.parse((String) sfItem.getField("Start_Time__c")));

            if (startTime.getTimeInMillis() > now.getTimeInMillis()) {
                ebayItem.setScheduleTime(startTime);
            }
        }

        // No returns accepted
        ReturnPolicyType returnPolicy = new ReturnPolicyType();
        returnPolicy.setReturnsAcceptedOption(ReturnsAcceptedOptionsCodeType.RETURNS_NOT_ACCEPTED.value());
        ebayItem.setReturnPolicy(returnPolicy);

        // Set paypal buying method
        BuyerPaymentMethodCodeType paypal = BuyerPaymentMethodCodeType.PAY_PAL;
        ebayItem.setPaymentMethods(new BuyerPaymentMethodCodeType[] {paypal});

        // Long list of shipping details
        ShippingDetailsType shippingDetails = new ShippingDetailsType();
        shippingDetails.setShippingType(ShippingTypeCodeType.FLAT);
        List<ShippingServiceOptionsType> serviceOptions = new ArrayList<>();
        List<InternationalShippingServiceOptionsType> internationalOptions = new ArrayList<>();
        if (sfItem.getField("Domestic_Shipping__c") != null) {
            ShippingServiceOptionsType domesticService = new ShippingServiceOptionsType();
            domesticService.setShippingService(ShippingServiceCodeType.FED_EX_HOME_DELIVERY.value());
            domesticService.setShippingServicePriority(1);
            AmountType serviceCost = new AmountType();
            serviceCost.setValue(Double.valueOf(sfItem.getField("Domestic_Shipping_Rate__c").toString()));
            domesticService.setShippingServiceCost(serviceCost);
            serviceOptions.add(domesticService);
        }
        if (sfItem.getField("International_Shipping_1__c") != null) {
            List<String> shipToLocations = new ArrayList<>();
            if (Boolean.valueOf(sfItem.getField("N_and_S_America_1__c").toString())) shipToLocations.add(ShippingRegionCodeType.AMERICAS.value());
            if (Boolean.valueOf(sfItem.getField("Brazil_1__c").toString())) shipToLocations.add(CountryCodeType.BR.value());
            if (Boolean.valueOf(sfItem.getField("Europe_1__c").toString())) shipToLocations.add(ShippingRegionCodeType.EUROPE.value());
            if (Boolean.valueOf(sfItem.getField("Germany_1__c").toString())) shipToLocations.add(CountryCodeType.DE.value());
            if (Boolean.valueOf(sfItem.getField("United_Kingdom_1__c").toString())) shipToLocations.add(CountryCodeType.GB.value());
            if (Boolean.valueOf(sfItem.getField("China_1__c").toString())) shipToLocations.add(CountryCodeType.CN.value());
            if (Boolean.valueOf(sfItem.getField("Japan_1__c").toString())) shipToLocations.add(CountryCodeType.JP.value());
            if (Boolean.valueOf(sfItem.getField("Canada_1__c").toString())) shipToLocations.add(CountryCodeType.CA.value());
            if (Boolean.valueOf(sfItem.getField("Mexico_1__c").toString())) shipToLocations.add(CountryCodeType.MX.value());
            if (Boolean.valueOf(sfItem.getField("Russian_Federation_1__c").toString())) shipToLocations.add(CountryCodeType.RU.value());
            if (Boolean.valueOf(sfItem.getField("France_1__c").toString())) shipToLocations.add(CountryCodeType.FR.value());
            if (Boolean.valueOf(sfItem.getField("Asia_1__c").toString())) shipToLocations.add(ShippingRegionCodeType.ASIA.value());
            if (Boolean.valueOf(sfItem.getField("Australia_1__c").toString())) shipToLocations.add(CountryCodeType.AU.value());
            InternationalShippingServiceOptionsType internationalService1 = new InternationalShippingServiceOptionsType();
            if ("Fedex Ground International for Canada".equals(sfItem.getField("International_Shipping_1__c").toString())) {
                internationalService1.setShippingService("FedExGroundInternationalToCanada");
            }
            internationalService1.setShipToLocation(shipToLocations.toArray(new String[shipToLocations.size()]));
            internationalService1.setShippingServicePriority(1);
            AmountType serviceCost = new AmountType();
            serviceCost.setValue(Double.valueOf(sfItem.getField("International_Shipping_Rate_1__c").toString()));
            internationalService1.setShippingServiceCost(serviceCost);
            internationalOptions.add(internationalService1);
        }
        if (sfItem.getField("International_Shipping_2__c") != null) {
            List<String> shipToLocations = new ArrayList<>();
            if (Boolean.valueOf(sfItem.getField("N_and_S_America_2__c").toString())) shipToLocations.add(ShippingRegionCodeType.AMERICAS.value());
            if (Boolean.valueOf(sfItem.getField("Brazil_2__c").toString())) shipToLocations.add(CountryCodeType.BR.value());
            if (Boolean.valueOf(sfItem.getField("Europe_2__c").toString())) shipToLocations.add(ShippingRegionCodeType.EUROPE.value());
            if (Boolean.valueOf(sfItem.getField("Germany_2__c").toString())) shipToLocations.add(CountryCodeType.DE.value());
            if (Boolean.valueOf(sfItem.getField("United_Kingdom_2__c").toString())) shipToLocations.add(CountryCodeType.GB.value());
            if (Boolean.valueOf(sfItem.getField("China_2__c").toString())) shipToLocations.add(CountryCodeType.CN.value());
            if (Boolean.valueOf(sfItem.getField("Japan_2__c").toString())) shipToLocations.add(CountryCodeType.JP.value());
            if (Boolean.valueOf(sfItem.getField("Canada_2__c").toString())) shipToLocations.add(CountryCodeType.CA.value());
            if (Boolean.valueOf(sfItem.getField("Mexico_2__c").toString())) shipToLocations.add(CountryCodeType.MX.value());
            if (Boolean.valueOf(sfItem.getField("Russian_Federation_2__c").toString())) shipToLocations.add(CountryCodeType.RU.value());
            if (Boolean.valueOf(sfItem.getField("France_2__c").toString())) shipToLocations.add(CountryCodeType.FR.value());
            if (Boolean.valueOf(sfItem.getField("Asia_2__c").toString())) shipToLocations.add(ShippingRegionCodeType.ASIA.value());
            if (Boolean.valueOf(sfItem.getField("Australia_2__c").toString())) shipToLocations.add(CountryCodeType.AU.value());
            InternationalShippingServiceOptionsType internationalService2 = new InternationalShippingServiceOptionsType();
            if ("FedEx International Priority".equals(sfItem.getField("International_Shipping_2__c").toString())) {
                internationalService2.setShippingService(ShippingServiceCodeType.FED_EX_INTERNATIONAL_PRIORITY.value());
            } else if ("FedEx International Economy".equals(sfItem.getField("International_Shipping_2__c").toString())){
                internationalService2.setShippingService(ShippingServiceCodeType.FED_EX_INTERNATIONAL_ECONOMY.value());
            }
            internationalService2.setShipToLocation(shipToLocations.toArray(new String[shipToLocations.size()]));
            internationalService2.setShippingServicePriority(2);
            AmountType serviceCost = new AmountType();
            serviceCost.setValue(Double.valueOf(sfItem.getField("International_Shipping_Rate_2__c").toString()));
            internationalService2.setShippingServiceCost(serviceCost);
            internationalOptions.add(internationalService2);
        }
        if (sfItem.getField("International_Shipping_3__c") != null) {
            List<String> shipToLocations = new ArrayList<>();
            if (Boolean.valueOf(sfItem.getField("N_and_S_America_3__c").toString())) shipToLocations.add(ShippingRegionCodeType.AMERICAS.value());
            if (Boolean.valueOf(sfItem.getField("Brazil_3__c").toString())) shipToLocations.add(CountryCodeType.BR.value());
            if (Boolean.valueOf(sfItem.getField("Europe_3__c").toString())) shipToLocations.add(ShippingRegionCodeType.EUROPE.value());
            if (Boolean.valueOf(sfItem.getField("Germany_3__c").toString())) shipToLocations.add(CountryCodeType.DE.value());
            if (Boolean.valueOf(sfItem.getField("United_Kingdom_3__c").toString())) shipToLocations.add(CountryCodeType.GB.value());
            if (Boolean.valueOf(sfItem.getField("China_3__c").toString())) shipToLocations.add(CountryCodeType.CN.value());
            if (Boolean.valueOf(sfItem.getField("Japan_3__c").toString())) shipToLocations.add(CountryCodeType.JP.value());
            if (Boolean.valueOf(sfItem.getField("Canada_3__c").toString())) shipToLocations.add(CountryCodeType.CA.value());
            if (Boolean.valueOf(sfItem.getField("Mexico_3__c").toString())) shipToLocations.add(CountryCodeType.MX.value());
            if (Boolean.valueOf(sfItem.getField("Russian_Federation_3__c").toString())) shipToLocations.add(CountryCodeType.RU.value());
            if (Boolean.valueOf(sfItem.getField("France_3__c").toString())) shipToLocations.add(CountryCodeType.FR.value());
            if (Boolean.valueOf(sfItem.getField("Asia_3__c").toString())) shipToLocations.add(ShippingRegionCodeType.ASIA.value());
            if (Boolean.valueOf(sfItem.getField("Australia_3__c").toString())) shipToLocations.add(CountryCodeType.AU.value());
            InternationalShippingServiceOptionsType internationalService3 = new InternationalShippingServiceOptionsType();
            if ("FedEx International Priority".equals(sfItem.getField("International_Shipping_3__c").toString())) {
                internationalService3.setShippingService(ShippingServiceCodeType.FED_EX_INTERNATIONAL_PRIORITY.value());
            } else if ("FedEx International Economy".equals(sfItem.getField("International_Shipping_3__c").toString())){
                internationalService3.setShippingService(ShippingServiceCodeType.FED_EX_INTERNATIONAL_ECONOMY.value());
            }
            internationalService3.setShipToLocation(shipToLocations.toArray(new String[shipToLocations.size()]));
            internationalService3.setShippingServicePriority(3);
            AmountType serviceCost = new AmountType();
            serviceCost.setValue(Double.valueOf(sfItem.getField("International_Shipping_Rate_3__c").toString()));
            internationalService3.setShippingServiceCost(serviceCost);
            internationalOptions.add(internationalService3);
        }

        shippingDetails.setShippingServiceOptions(serviceOptions.toArray(new ShippingServiceOptionsType[serviceOptions.size()]));
        shippingDetails.setInternationalShippingServiceOption(internationalOptions.toArray(new InternationalShippingServiceOptionsType[internationalOptions.size()]));

        ebayItem.setShippingDetails(shippingDetails);

        // Long list of attributes based on record type
        NameValueListArrayType information = new NameValueListArrayType();
        List<NameValueListType> info = new ArrayList<>();
        // Product Information
        if(sfItem.getField("Designer__c") != null) info.add(createNameValue("Designer", new String[] {(String)sfItem.getField("Designer_Name__c")}));
        if(sfItem.getField("Style__c") != null) info.add(createNameValue("Style/Shape", new String[] {(String)sfItem.getField("Style_Shape__c")}));
        if(sfItem.getField("Color__c") != null) info.add(createNameValue("Color", new String[] {(String)sfItem.getField("Color__c")}));
        if(sfItem.getField("Size__c") != null) info.add(createNameValue("Size", new String[] {(String)sfItem.getField("Size__c")}));
        // Basic Information
        if(sfItem.getField("Made_In__c") != null) info.add(createNameValue("Made In", new String[] {(String)sfItem.getField("Made_In__c")}));
        if(sfItem.getField("Fabric_Content__c") != null) info.add(createNameValue("Fabric Content", new String[] {(String)sfItem.getField("Fabric_Content__c")}));
        if(sfItem.getField("Fabric_Care__c") != null) info.add(createNameValue("Fabric Care", new String[] {(String)sfItem.getField("Fabric_Care__c")}));
        if(sfItem.getField("Measurements__c") != null) info.add(createNameValue("Measurements", new String[] {(String)sfItem.getField("Measurements__c")}));
        switch (recordTypeDevName) {
            case "Bag":
                if(sfItem.getField("Height__c") != null) info.add(createNameValue("Height", new String[] {(String)sfItem.getField("Height__c")}));
                if(sfItem.getField("Length__c") != null) info.add(createNameValue("Length", new String[] {(String)sfItem.getField("Length__c")}));
                if(sfItem.getField("Width__c") != null) info.add(createNameValue("Width", new String[] {(String)sfItem.getField("Width__c")}));
                if(sfItem.getField("Handle_Length__c") != null) info.add(createNameValue("Handle Length", new String[] {(String)sfItem.getField("Handle_Length__c")}));
                if(sfItem.getField("Handle_Drop__c") != null) info.add(createNameValue("Handle Drop", new String[] {(String)sfItem.getField("Handle_Drop__c")}));
                if(sfItem.getField("Strap_Length__c") != null) info.add(createNameValue("Strap Length", new String[] {(String)sfItem.getField("Strap_Length__c")}));
                if(sfItem.getField("Strap_Drop__c") != null) info.add(createNameValue("Strap Drop", new String[] {(String)sfItem.getField("Strap_Drop__c")}));
                break;
            case "Belt":
                if(sfItem.getField("Total_Length__c") != null) info.add(createNameValue("Total Length", new String[] {(String)sfItem.getField("Total_Length__c")}));
                if(sfItem.getField("Width__c") != null) info.add(createNameValue("Width", new String[] {(String)sfItem.getField("Width__c")}));
                if(sfItem.getField("Buckle_Length__c") != null) info.add(createNameValue("Buckle Length", new String[] {(String)sfItem.getField("Buckle_Length__c")}));
                if(sfItem.getField("Buckle_Height__c") != null) info.add(createNameValue("Buckle Height", new String[] {(String)sfItem.getField("Buckle_Height__c")}));
                break;
            case "Bracelet":
                if(sfItem.getField("Total_Length__c") != null) info.add(createNameValue("Total Length", new String[] {(String)sfItem.getField("Total_Length__c")}));
                if(sfItem.getField("Width__c") != null) info.add(createNameValue("Width", new String[] {(String)sfItem.getField("Width__c")}));
                if(sfItem.getField("Top_Opening_Diameter__c") != null) info.add(createNameValue("Top Opening Diameter", new String[] {(String)sfItem.getField("Top_Opening_Diameter__c")}));
                break;
            case "Clothes":
                if(sfItem.getField("Total_Length__c") != null) info.add(createNameValue("Total Length", new String[] {(String)sfItem.getField("Total_Length__c")}));
                if(sfItem.getField("Sleeve_Length__c") != null) info.add(createNameValue("Sleeve Length", new String[] {(String)sfItem.getField("Sleeve_Length__c")}));
                if(sfItem.getField("Shoulder_to_shoulder__c") != null) info.add(createNameValue("Shoulder To Shoulder", new String[] {(String)sfItem.getField("Shoulder_to_shoulder__c")}));
                if(sfItem.getField("Bust__c") != null) info.add(createNameValue("Bust", new String[] {(String)sfItem.getField("Bust__c")}));
                if(sfItem.getField("Waist__c") != null) info.add(createNameValue("Waist", new String[] {(String)sfItem.getField("Waist__c")}));
                if(sfItem.getField("Hips__c") != null) info.add(createNameValue("Hips", new String[] {(String)sfItem.getField("Hips__c")}));
                break;
            case "ClothesPants":
                if(sfItem.getField("Total_Length__c") != null) info.add(createNameValue("Total Length", new String[] {(String)sfItem.getField("Total_Length__c")}));
                if(sfItem.getField("Waist__c") != null) info.add(createNameValue("Waist", new String[] {(String)sfItem.getField("Waist__c")}));
                if(sfItem.getField("Hips__c") != null) info.add(createNameValue("Hips", new String[] {(String)sfItem.getField("Hips__c")}));
                if(sfItem.getField("Inseam__c") != null) info.add(createNameValue("Inseam", new String[] {(String)sfItem.getField("Inseam__c")}));
                if(sfItem.getField("Rise__c") != null) info.add(createNameValue("Rise", new String[] {(String)sfItem.getField("Rise__c")}));
                if(sfItem.getField("Leg_Opening__c") != null) info.add(createNameValue("Leg Opening", new String[] {(String)sfItem.getField("Leg_Opening__c")}));
            case "Earrings":
                if(sfItem.getField("Total_Height__c") != null) info.add(createNameValue("Total Height", new String[] {(String)sfItem.getField("Total_Height__c")}));
                if(sfItem.getField("Length__c") != null) info.add(createNameValue("Length", new String[] {(String)sfItem.getField("Length__c")}));
                break;
            case "Necklace":
                if(sfItem.getField("Total_Length__c") != null) info.add(createNameValue("Total Length", new String[] {(String)sfItem.getField("Total_Length__c")}));
                if(sfItem.getField("Necklace_Drop__c") != null) info.add(createNameValue("Necklace Drop", new String[] {(String)sfItem.getField("Necklace_Drop__c")}));
                if(sfItem.getField("Chain_Width__c") != null) info.add(createNameValue("Chain Width", new String[] {(String)sfItem.getField("Chain_Width__c")}));
                if(sfItem.getField("Pendant_Height__c") != null) info.add(createNameValue("Pendant Height", new String[] {(String)sfItem.getField("Pendant_Height__c")}));
                if(sfItem.getField("Pendant_Length__c") != null) info.add(createNameValue("Pendant Length", new String[] {(String)sfItem.getField("Pendant_Length__c")}));
                break;
            case "Ring":
                if(sfItem.getField("Total_Length__c") != null) info.add(createNameValue("Total Length", new String[] {(String)sfItem.getField("Total_Length__c")}));
                if(sfItem.getField("Shank_Height__c") != null) info.add(createNameValue("Shank Height", new String[] {(String)sfItem.getField("Shank_Height__c")}));
                if(sfItem.getField("Top_Opening_Diameter__c") != null) info.add(createNameValue("Top Opening Diameter", new String[] {(String)sfItem.getField("Top_Opening_Diameter__c")}));
                break;
            case "Scarf":
                if(sfItem.getField("Total_Length__c") != null) info.add(createNameValue("Total Length", new String[] {(String)sfItem.getField("Total_Length__c")}));
                if(sfItem.getField("Width__c") != null) info.add(createNameValue("Width", new String[] {(String)sfItem.getField("Width__c")}));
                break;
            case "Shoes":
                if(sfItem.getField("Insole_Length__c") != null) info.add(createNameValue("Insole Length", new String[] {(String)sfItem.getField("Insole_Length__c")}));
                if(sfItem.getField("Insole_Width__c") != null) info.add(createNameValue("Insole Width", new String[] {(String)sfItem.getField("Insole_Width__c")}));
                if(sfItem.getField("Outsole_Length__c") != null) info.add(createNameValue("Outsole Length", new String[] {(String)sfItem.getField("Outsole_Length__c")}));
                if(sfItem.getField("Outsole_Width__c") != null) info.add(createNameValue("Outsold Width", new String[] {(String)sfItem.getField("Outsole_Width__c")}));
                if(sfItem.getField("Platform_Height__c") != null) info.add(createNameValue("Platform Height", new String[] {(String)sfItem.getField("Platform_Height__c")}));
                if(sfItem.getField("Heel_Height__c") != null) info.add(createNameValue("Heel Height", new String[] {(String)sfItem.getField("Heel_Height__c")}));
                if(sfItem.getField("Total_Height__c") != null) info.add(createNameValue("Total Height", new String[] {(String)sfItem.getField("Total_Height__c")}));
                if(sfItem.getField("Top_Opening_Circumference__c") != null) info.add(createNameValue("Top Opening Circumference", new String[] {(String)sfItem.getField("Top_Opening_Circumference__c")}));
                break;
            case "Sunglasses":
                if(sfItem.getField("Frame_Height__c") != null) info.add(createNameValue("Frame Height", new String[] {(String)sfItem.getField("Frame_Height__c")}));
                if(sfItem.getField("Frame_Length__c") != null) info.add(createNameValue("Frame Length", new String[] {(String)sfItem.getField("Frame_Length__c")}));
                if(sfItem.getField("Lens_Height__c") != null) info.add(createNameValue("Lens Height", new String[] {(String)sfItem.getField("Lens_Height__c")}));
                if(sfItem.getField("Lens_Length__c") != null) info.add(createNameValue("Lens Length", new String[] {(String)sfItem.getField("Lens_Length__c")}));
                if(sfItem.getField("Arm_Height__c") != null) info.add(createNameValue("Arm Height", new String[] {(String)sfItem.getField("Arm_Height__c")}));
                if(sfItem.getField("Arm_Length__c") != null) info.add(createNameValue("Arm Length", new String[] {(String)sfItem.getField("Arm_Length__c")}));
                break;
            case "Watch":
                if(sfItem.getField("Total_Length__c") != null) info.add(createNameValue("Total Length", new String[] {(String)sfItem.getField("Total_Length__c")}));
                if(sfItem.getField("Band_Width__c") != null) info.add(createNameValue("Band Width", new String[] {(String)sfItem.getField("Band_Width__c")}));
                if(sfItem.getField("Face_Height__c") != null) info.add(createNameValue("Face Height", new String[] {(String)sfItem.getField("Face_Height__c")}));
                if(sfItem.getField("Face_Length__c") != null) info.add(createNameValue("Face Length", new String[] {(String)sfItem.getField("Face_Length__c")}));
                if(sfItem.getField("Case_Height__c") != null) info.add(createNameValue("Case Height", new String[] {(String)sfItem.getField("Case_Height__c")}));
                if(sfItem.getField("Case_Length__c") != null) info.add(createNameValue("Case Length", new String[] {(String)sfItem.getField("Case_Length__c")}));
        }

        //Add Item #/SKU to the item
        ebayItem.setSKU((String)sfItem.getField("Name"));

        // Set upc to does not apply
        ProductListingDetailsType productListingDetailsType = new ProductListingDetailsType();
        productListingDetailsType.setUPC("Does Not Apply");
        productListingDetailsType.setEAN("Does Not Apply");
        productListingDetailsType.setISBN("Does Not Apply");
        ebayItem.setProductListingDetails(productListingDetailsType);

        // Parsing category specifics from JSON
        if (null != sfItem.getField("item_specific_json__c")) {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode node = mapper.readTree((String)sfItem.getField("item_specific_json__c"));
            if (node.size() > 0) {
                Iterator<JsonNode> iterator = node.getElements();
                List<String> values = new ArrayList<>();
                while(iterator.hasNext()) {
                    JsonNode innerNode = iterator.next();
                    Iterator<JsonNode> innerIterator = innerNode.get("values").getElements();
                    if (innerIterator.hasNext()) { // only one value
                        JsonNode text = innerIterator.next();
                        values.add(text.getValueAsText().replaceAll("\"", ""));
                    }
                    info.add(createNameValue(innerNode.get("name").toString().replaceAll("\"", ""), values.toArray(new String[values.size()])));
                    values.clear();
                }
            }
        }
        information.setNameValueList(info.toArray(new NameValueListType[info.size()]));
        ebayItem.setItemSpecifics(information);

        // Clear Seller profiles because its giving us errors, this will default once its relisted
        ebayItem.setSellerProfiles(null);

        return ebayItem;
    }

    /**
     * This method uploads and attaches pictures the the selected ebay item.
     * @param item The ebay item to which pictures are attached
     * @param attachments The List of pictures to attach to an item
     * @return Item with pictures attached
     * @throws IOException
     */
    public ItemType attachPictures(ItemType item, List<SObject> attachments) throws IOException {
        eBayPictureServiceXMLCall pictureCall = new eBayPictureServiceXMLCall(ebay);
        List<String> urls = new ArrayList<>();

        for (SObject attachment : attachments) {
            List<String> tempList = new ArrayList<>();
            tempList.add(siteUrl + "servlet/servlet.FileDownload?file=" + attachment.getId());
            UploadSiteHostedPicturesRequestType rType = new UploadSiteHostedPicturesRequestType();
            rType.setExternalPictureURL(tempList.toArray(new String[tempList.size()]));
            UploadSiteHostedPicturesResponseType response = pictureCall.uploadSiteHostedPictures(rType);
            if (response.getSiteHostedPictureDetails() != null) {
                urls.add(response.getSiteHostedPictureDetails().getFullURL());
            }
        }

        PictureDetailsType pictureDetailsType = new PictureDetailsType();
        pictureDetailsType.setPictureURL(urls.toArray(new String[urls.size()]));
        item.setPictureDetails(pictureDetailsType);
        return item;
    }

    /**
     * Lists an item on ebay.
     * @param item The Item to list.
     * @return Fees associated to listing an item.
     * @throws Exception
     */
    public Calendar listItem(ItemType item) throws Exception {
        AddItemCall call = new AddItemCall(ebay);
        call.setItem(item);
        call.addItem();
        return call.getReturnedEndTime();
    }

    /**
     * Returns the status of an item
     * @param listingNumber the ebay item id
     * @return The ebay item.
     * @throws Exception
     */
    public ItemType getItem(String listingNumber) throws Exception {
        if (listingNumber == null) { return null; }
        GetItemCall itemcall = new GetItemCall(ebay);
        itemcall.setDetailLevel(new DetailLevelCodeType[]{DetailLevelCodeType.RETURN_ALL});
        itemcall.setItemID(listingNumber);
        itemcall.setIncludeWatchCount(true);
        return itemcall.getItem();
    }

    /**
     * Get transaction information for an ebay item.
     * @param listingNumber The ebay item id.
     * @return A list of ebay transactions.
     * @throws Exception
     */
    public OrderArrayType getItemTransactions(String listingNumber) throws Exception {
        GetItemTransactionsCall transactionsCall = new GetItemTransactionsCall(ebay);
        transactionsCall.setItemID(listingNumber);
        Calendar now = Calendar.getInstance();
        Calendar past = Calendar.getInstance();
        past.setTime(now.getTime());
        past.add(Calendar.DATE, -29);
        TimeFilter modTimeFilter = new TimeFilter(past, now);
        transactionsCall.setModifiedTimeFilter(modTimeFilter);
        TransactionType[] transactions = transactionsCall.getItemTransactions();
        if (transactions == null) {return null;}

        GetOrderTransactionsCall orderTransactionsCall = new GetOrderTransactionsCall(ebay);
        ItemTransactionIDArrayType transactionIdArray = new ItemTransactionIDArrayType();
        ItemTransactionIDType transactionId = new ItemTransactionIDType();
        transactionId.setItemID(listingNumber);
        transactionId.setTransactionID(transactions[transactions.length - 1].getTransactionID());
        transactionIdArray.setItemTransactionID(new ItemTransactionIDType[] {transactionId});
        orderTransactionsCall.setItemTransactionIDArray(transactionIdArray);
        orderTransactionsCall.setDetailLevel(new DetailLevelCodeType[]{DetailLevelCodeType.RETURN_ALL});
        return orderTransactionsCall.getOrderTransactions();
    }

    /**
     * Ends an active listing.
     * @param itemId The ebay item id.
     * @return The end time for the item.
     * @throws Exception
     */
    public Calendar endItem(String itemId) throws Exception {
        EndItemCall call = new EndItemCall(ebay);
        call.setEndingReason(EndReasonCodeType.NOT_AVAILABLE);
        call.setItemID(itemId);
        return call.endItem();
    }

    /**
     * Relists an item on ebay.
     * @param ebayItem The ebay Item to be relisted.
     * @return The item end time.
     * @throws Exception
     */
    public Calendar relistItem(ItemType ebayItem) throws Exception {
        ShipPackageDetailsType packageDetailsType = new ShipPackageDetailsType();
        ebayItem.setShippingPackageDetails(packageDetailsType);
        RelistItemCall relistCall = new RelistItemCall(ebay);
        relistCall.setItemToBeRelisted(ebayItem);
        relistCall.relistItem();
        return relistCall.getReturnedEndTime();
    }

    /**
     * Revises an item on ebay.
     * @param ebayItem The ebay Item to be revised.
     * @return The item end time.
     * @throws Exception
     */
    public Calendar reviseItem(ItemType ebayItem) throws Exception {
        ShipPackageDetailsType packageDetailsType = new ShipPackageDetailsType();
        ebayItem.setShippingPackageDetails(packageDetailsType);
        ReviseItemCall reviseCall = new ReviseItemCall(ebay);
        reviseCall.setItemToBeRevised(ebayItem);
        reviseCall.reviseItem();
        return reviseCall.getReturnedEndTime();
    }

    /**
     * Get all items for which events have happened in ebay (bids, watches, items bought, seller updates, etc)
     * @return The list of items that have been bid on, bought, watched on ebay since lastQueriedTime
     * @throws Exception
     */
    public List<ItemType> getSellerEvents() throws Exception {
        GetSellerEventsCall getSellerEventsCall = new GetSellerEventsCall(ebay);
        getSellerEventsCall.setDetailLevel(new DetailLevelCodeType[]{DetailLevelCodeType.RETURN_ALL});
        Calendar now = Calendar.getInstance();
        Calendar past = Calendar.getInstance();
        //past.add(Calendar.MINUTE, -30); // Temporary fix when bulk update causes this to retrieve > 3000 seller events
        past.add(Calendar.HOUR, -2);
        TimeFilter modTimeFilter = new TimeFilter(past, now);
        getSellerEventsCall.setModTimeFilter(modTimeFilter);
        getSellerEventsCall.setIncludeWatchCount(true);
        CallRetry retry = new CallRetry();
        retry.setMaximumRetries(2);
        retry.setDelayTime(2000);
        getSellerEventsCall.setCallRetry(retry);
        return Arrays.asList(getSellerEventsCall.getSellerEvents());
    }

    /**
     * Get orders that have been placed in ebay.
     * @return The list of orders that have been placed within the time frame.
     * @throws Exception
     */
    public List<OrderType> getOrders() throws Exception {
        List<OrderType> orders = new ArrayList<>();
        int page = 1;
        GetOrdersCall ordersCall = new GetOrdersCall(ebay);
        ordersCall.setDetailLevel(new DetailLevelCodeType[]{DetailLevelCodeType.RETURN_ALL});
        Calendar now = Calendar.getInstance();
        Calendar past = Calendar.getInstance();
        past.add(Calendar.HOUR, -2);
        ordersCall.setModTimeFrom(past);
        ordersCall.setModTimeTo(now);
        PaginationType paginationType = new PaginationType();
        paginationType.setPageNumber(page);
        ordersCall.setPagination(paginationType);
        OrderType[] orderTypes = ordersCall.getOrders();
        for (OrderType order : orderTypes) {
            orders.add(order);
        }
        page++;
        while(ordersCall.getReturnedHasMoreOrders()) {
            ordersCall = new GetOrdersCall(ebay);
            ordersCall.setDetailLevel(new DetailLevelCodeType[]{DetailLevelCodeType.RETURN_ALL});
            ordersCall.setModTimeFrom(past);
            ordersCall.setModTimeTo(now);
            paginationType.setPageNumber(page);
            ordersCall.setPagination(paginationType);
            orderTypes = ordersCall.getOrders();
            for (OrderType order : orderTypes) {
                orders.add(order);
            }
            page++;
        }
        return orders;
    }

    /**
     * Get messages from ebay.
     * @return A list of ebay messages
     * @throws Exception
     */
    public List<MyMessagesMessageType> getMessages() throws Exception {
        GetMyMessagesCall call = new GetMyMessagesCall(ebay);
        call.setDetailLevel(new DetailLevelCodeType[]{DetailLevelCodeType.RETURN_HEADERS});
        Calendar now = Calendar.getInstance();
        Calendar past = Calendar.getInstance();
        past.setTime(now.getTime());
        past.add(Calendar.DAY_OF_YEAR, -1);
        call.setStartTime(past);
        call.setEndTime(now);
        call.getMyMessages();

        // Since ebay has a 10 messageId limit for calls, break them up.
        MyMessagesMessageType[] messages = call.getReturnedMyMessages();
        List<MyMessagesMessageType> messagesWithBody = new ArrayList<>();
        int totalLoops = messages.length / 10 + (messages.length % 10 != 0 ? 1 : 0);
        for (int loop = 0; loop < totalLoops; loop++) {
            List<String> messageIds = new ArrayList<>();
            for (int i = 0; i < 10; i++) { // only get 10 messages at a time
                if (10 * loop + i > messages.length - 1) { break; }
                messageIds.add(messages[10 * loop + i].getMessageID());
            }
            call = new GetMyMessagesCall(ebay);
            call.setMessageIDs(messageIds.toArray(new String[messageIds.size()]));
            call.setDetailLevel(new DetailLevelCodeType[]{DetailLevelCodeType.RETURN_MESSAGES});
            call.getMyMessages();
            messagesWithBody.addAll(Arrays.asList(call.getReturnedMyMessages()));
        }
        return messagesWithBody;
    }

    /**
     * Setting the Tracking information for the item in ebay after it has been shipped.
     * @param sfItem The item for which to send tracking information.
     */
    public void sendTrackingInfo(SObject sfItem) throws Exception {
        CompleteSaleCall completeSaleCall = new CompleteSaleCall(ebay);
        completeSaleCall.setItemID((String)sfItem.getField("Ebay_Listing_Number__c"));
        ShipmentType shipmentType = new ShipmentType();
        shipmentType.setShipmentTrackingNumber(((XmlObject)sfItem.getField("Shipment__r")).getField("Tracking_Number__c").toString());
        shipmentType.setShippingCarrierUsed(((XmlObject)sfItem.getField("Shipment__r")).getField("Shipping_Type__c").toString());
        completeSaleCall.setShipment(shipmentType);
        completeSaleCall.completeSale();
    }

    private NameValueListType createNameValue(String name, String[] values) {
        NameValueListType nameValue = new NameValueListType();
        nameValue.setName(name);
        nameValue.setValue(values);
        return nameValue;
    }
}
