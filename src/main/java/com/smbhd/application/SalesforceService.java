package com.smbhd.application;

import com.ebay.soap.eBLBaseComponents.ItemType;
import com.ebay.soap.eBLBaseComponents.OrderType;
import com.ebay.soap.eBLBaseComponents.TransactionType;
import com.google.common.collect.Lists;
import com.sforce.soap.partner.*;
import com.sforce.soap.partner.Error;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;
import com.sforce.ws.bind.XmlObject;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class SalesforceService {

    private PartnerConnection salesforce;
    @Value("${salesforce.siteurl}")
    private String siteUrl;
    private static Map<String, String> recordTypeIdToDevName;
    private static String QUERY;
    private static Map<String, Field> itemFieldsByName = new HashMap<>();

    @Autowired
    public SalesforceService(@Value("${salesforce.username}") String salesforceUsername, @Value("${salesforce.passwordToken}") String salesforcePasswordToken) throws ConnectionException {
        ConnectorConfig salesforceConfig = new ConnectorConfig();
        salesforceConfig.setUsername(salesforceUsername);
        salesforceConfig.setPassword(salesforcePasswordToken);
        salesforceConfig.setAuthEndpoint("https://login.salesforce.com/services/Soap/u/35.0/");
        salesforce = Connector.newConnection(salesforceConfig);

        QUERY = "Select ";
        for (Field field : salesforce.describeSObject("Item__c").getFields()) {
            itemFieldsByName.put(field.getName(), field);
            QUERY += field.getName() + ", ";
        }
        QUERY += "Shipment__r.Tracking_Number__c, Shipment__r.Shipping_Type__c, (Select id, name from Attachments limit 12) from Item__c";
    }

    /**
     * Retrieves a list of salesforce items that need to be listed.
     * @return A List of Salesforce items
     * @throws ConnectionException
     */
    public List<SObject> getItemsToList() throws ConnectionException {
        QueryResult salesforceQuery = salesforce.query(QUERY + " where item_status__c = 'Scheduled for Ebay' AND RelistListingMarked__c = false AND ReviseListingMarked__c = false AND Applicaiton_Error_Time__c = null order by name asc limit 10");
        return (List<SObject>)(List<?>) Arrays.asList(salesforceQuery.getRecords());
    }
    /**
     * Retrieves a list of salesforce items that need updating or listing.
     * @return A List of Salesforce items
     * @throws ConnectionException
     */
    public List<SObject> getItemsForEbay() throws ConnectionException {
        QueryResult salesforceQuery = salesforce.query(QUERY + " where Applicaiton_Error_Time__c = null AND ((item_status__c = 'Scheduled for Ebay' AND (RelistListingMarked__c = true OR ReviseListingMarked__c = true)) " +
                "OR EndListingMarked__c = true OR (Ebay_Listing_Number__c != null AND Tracking_Sent_To_Ebay__c = false AND Shipment__r.Tracking_Number__c != null AND Shipment__r.Shipping_Type__c != null)) order by name asc");
        return (List<SObject>)(List<?>) Arrays.asList(salesforceQuery.getRecords());
    }
    /**
     * Retrieves a list of salesforce items which are passed in from parameter
     * @return A List of Salesforce items
     * @throws ConnectionException
     */
    public List<SObject> getItemsFromList(List<String> ids) throws ConnectionException {
        QueryResult salesforceQuery = salesforce.query(QUERY + " where Id IN ('"+String.join("','", ids)+"')");
        return (List<SObject>)(List<?>) Arrays.asList(salesforceQuery.getRecords());
    }

    /**
     * Get salesforce recordType Names for template use
     * @param recordTypeId Salesforce recordType Id
     * @return A RecordType Name
     * @throws ConnectionException
     */
    public String getRecordTypeDevName(String recordTypeId) throws ConnectionException {
        if (recordTypeIdToDevName == null) {
            recordTypeIdToDevName = new HashMap<>();
            QueryResult salesforceQuery = salesforce.query("Select id, developername from RecordType where sobjecttype = 'Item__c'");
            for (SObject recordType : (List<SObject>) (List<?>) Arrays.asList(salesforceQuery.getRecords())) {
                recordTypeIdToDevName.put(recordType.getId(), (String)recordType.getField("DeveloperName"));
            }
        }
        return recordTypeIdToDevName.get(recordTypeId);
    }

    /**
     * Get the html template for the item. It should be corresponding to the itemType.
     * @param sfItem Item to get template from.
     * @return A string representing the template.
     * @throws IOException
     */
    public String getTemplateHTML(SObject sfItem, ItemType ebayItem) throws ConnectionException {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(siteUrl + "ebayTemplate_" + sfItem.getField("Item_Type__c") + "?id=" + sfItem.getId());
        StringBuilder result = new StringBuilder();

        try {
            HttpResponse response = client.execute(request);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        }
        catch(IOException e) {
            throw new ConnectionException(e.getMessage());
        }
        // we "sanitize" the output to make it nice for ebay.
        String theTemplate = result.toString().replace("display:none !important;", "").replaceFirst("<head>(.*?)</head>", "<head></head>");
        if (ebayItem.getPictureDetails() != null) {
            theTemplate = theTemplate.replace("{{IMAGES_URLS}}", String.join("|", ebayItem.getPictureDetails().getPictureURL()));
        }

        return theTemplate;
    }

    /**
     * Get Item Images.
     * @param sfItem The item list to get attachments from.
     * @return A List of Images.
     * @throws ConnectionException
     */
    public List<SObject> getItemAttachments(SObject sfItem) throws ConnectionException {
        List<SObject> attachments = new ArrayList<>();
        List<XmlObject> attachmentsXml = Lists.newArrayList(sfItem.getChild("Attachments").getChildren());
        if (attachmentsXml == null || attachmentsXml.size() == 0) {
            return attachments;
        }
        for (XmlObject xmlObj : attachmentsXml) {
            if (xmlObj.getName().getLocalPart().equals("records")) {
                List<XmlObject> fields = Lists.newArrayList(xmlObj.getChildren());
                SObject attachment = new SObject();
                attachment.setType("Attachment");

                for (XmlObject field : fields) {
                    if (field.getName().getLocalPart().equalsIgnoreCase("id")) {
                        attachment.setId(field.getValue().toString());
                    }
                    else if (field.getName().getLocalPart().equalsIgnoreCase("name")) {
                        attachment.setField("Name", field.getValue().toString());
                    }
                }

                attachments.add(attachment);
            }
        }
        return attachments;
    }

    /**
     * Updates items on salesforce based on information from Ebay.
     * @param sfItems The items to update on Salesforce
     * @throws ConnectionException
     */
    public void updateItems(List<SObject> sfItems) throws ConnectionException {
        //Remove non updatable fields
        for (SObject sfItem : sfItems) {

            sfItem.removeField("Id");

            List<XmlObject> fields = Lists.newArrayList(sfItem.getChildren());
            for (XmlObject field : fields) {
                String fieldName = field.getName().getLocalPart();

                if (!fieldName.equalsIgnoreCase("id") && !fieldName.equalsIgnoreCase("type")) {
                    if (field.getValue() == null || !itemFieldsByName.containsKey(fieldName) || !itemFieldsByName.get(fieldName).isUpdateable()) {
                        sfItem.removeField(fieldName);
                    }
                    else if (itemFieldsByName.containsKey(fieldName)) {
                        if (itemFieldsByName.get(fieldName).getType() == FieldType._boolean) {
                            sfItem.setField(fieldName, Boolean.valueOf(sfItem.getField(fieldName).toString()));
                        }
                        else if (itemFieldsByName.get(fieldName).getType() == FieldType.datetime && sfItem.getField(fieldName) instanceof String) {
                            Calendar cal = Calendar.getInstance();
                            SimpleDateFormat sfdcDateTimeParser = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");
                            sfdcDateTimeParser.setTimeZone(TimeZone.getTimeZone("GMT"));
                            try {
                                cal.setTime(sfdcDateTimeParser.parse(sfItem.getField(fieldName).toString()));
                            }
                            catch(ParseException e) {
                                throw new ConnectionException(e.getMessage());
                            }
                            sfItem.setField(fieldName, cal);
                        }
                        else if (itemFieldsByName.get(fieldName).getType() == FieldType.date && sfItem.getField(fieldName) instanceof String) {
                            Calendar cal = Calendar.getInstance();
                            SimpleDateFormat sfdcDateTimeParser = new SimpleDateFormat("yyyy-MM-dd");
                            sfdcDateTimeParser.setTimeZone(TimeZone.getTimeZone("GMT"));
                            try {
                                cal.setTime(sfdcDateTimeParser.parse(sfItem.getField(fieldName).toString()));
                            }
                            catch(ParseException e) {
                                throw new ConnectionException(e.getMessage());
                            }
                            sfItem.setField(fieldName, cal);
                        }
                    }

                }
            }

            sfItem.setType("Item__c");
        }
        SaveResult[] results = salesforce.update(sfItems.toArray(new SObject[sfItems.size()]));
        String errors = "";
        for (SaveResult result : results) {
            if (result.getErrors().length > 0) {
                for (Error error : result.getErrors()) {
                    errors += error.getMessage() + "<br/>";
                }
            }
        }
        if (errors.length() > 0) {
            throw new ConnectionException(errors);
        }
    }

    /**
     * Upserts messages on salesforce
     * @param sfMessages The messages on Salesforce
     * @throws ConnectionException
     */
    public void upsertMessages(List<SObject> sfMessages) throws ConnectionException {
        ArrayList<SObject> messagesToUpsert = new ArrayList<>();
        if (sfMessages.size() > 0) {
            String query = "Select id, Ebay_Message_Id__c from Message__c where Ebay_Message_Id__c in (";
            for (SObject msg : sfMessages) {
                query += "'" + msg.getField("Ebay_Message_Id__c") + "',";
            }
            query = query.substring(0, query.length() - 1) + ")";
            QueryResult salesforceQuery = salesforce.query(query);
            ArrayList<SObject> existingMessages = new ArrayList<>();
            existingMessages.addAll((List<SObject>) (List<?>) Arrays.asList(salesforceQuery.getRecords()));
            for (int i = existingMessages.size() - 1; i >= 0; i--) {
                for (SObject sfMessage : sfMessages) {
                    if (sfMessage.getField("Ebay_Message_Id__c").toString().equals(existingMessages.get(i).getField("Ebay_Message_Id__c"))) {
                        existingMessages.remove(i);
                        break;
                    }
                }
            }
            sfMessages.addAll(existingMessages);
        }
        messagesToUpsert.addAll(sfMessages);
        List<SObject> messageBatch = new ArrayList<>();
        for (SObject message : messagesToUpsert) {
            messageBatch.add(message);
            if (messageBatch.size() == 200) {
                UpsertResult[] results = salesforce.upsert("Ebay_Message_Id__c", messageBatch.toArray(new SObject[messageBatch.size()]));
                messageBatch.clear();
                String errors = "";
                for (UpsertResult result : results) {
                    if (result.getErrors().length > 0) {
                        for (Error error : result.getErrors()) {
                            errors += error.getMessage() + '\n';
                        }
                    }
                }
                if (errors.length() > 0) {
                    throw new ConnectionException(errors);
                }
            }
        }
        if (messageBatch.size() > 0) {
            UpsertResult[] results = salesforce.upsert("Ebay_Message_Id__c", messageBatch.toArray(new SObject[messageBatch.size()]));
            messageBatch.clear();
            String errors = "";
            for (UpsertResult result : results) {
                if (result.getErrors().length > 0) {
                    for (Error error : result.getErrors()) {
                        errors += error.getMessage() + '\n';
                    }
                }
            }
            if (errors.length() > 0) {
                throw new ConnectionException(errors);
            }
        }
    }

    /**
     * Retrieves ebay-listed items from salesforce
     * @return A list of saleforce items to update.
     * @throws ConnectionException
     */
    public ArrayList<SObject> getItemsToUpdate(List<ItemType> ebayItems) throws ConnectionException {
        ArrayList<SObject> items = new ArrayList<>();

        if (ebayItems.size() > 0) {
            if (ebayItems.size() >= 500) {
                List<ItemType> temp = new ArrayList<>();

                String query = QUERY + " where (item_status__c = 'Listed on eBay Only' OR item_status__c = 'Listed on eBay/Web' OR item_status__c = 'Listed on eBay, Pending Web Listing' OR item_status__c = 'Sold/Unpaid' OR item_status__c = 'Out on Lux Box') AND ( Ebay_Listing_Number__c in (";

                for (ItemType ebayItem : ebayItems) {
                    temp.add(ebayItem);
                    query += "'" + ebayItem.getItemID() + "',";
                    if (temp.size() == 500) {
                        query = query.substring(0, query.length() - 1) + "))";
                        QueryResult salesforceQuery = salesforce.query(query);
                        items.addAll((List<SObject>) (List<?>) Arrays.asList(salesforceQuery.getRecords()));

                        query = QUERY + " where (item_status__c = 'Listed on eBay Only' OR item_status__c = 'Listed on eBay/Web' OR item_status__c = 'Listed on eBay, Pending Web Listing' OR item_status__c = 'Sold/Unpaid' OR item_status__c = 'Out on Lux Box') AND ( Ebay_Listing_Number__c in (";
                        temp.clear();
                    }
                }
                if (temp.size() > 0) {
                    query = query.substring(0, query.length() - 1) + "))";
                    QueryResult salesforceQuery = salesforce.query(query);
                    items.addAll((List<SObject>) (List<?>) Arrays.asList(salesforceQuery.getRecords()));
                    temp.clear();
                }
            }
            else {
                String query = QUERY + " where (item_status__c = 'Listed on eBay Only' OR item_status__c = 'Listed on eBay/Web' OR item_status__c = 'Listed on eBay, Pending Web Listing' OR item_status__c = 'Sold/Unpaid' OR item_status__c = 'Out on Lux Box') AND ( Ebay_Listing_Number__c in (";
                for (ItemType ebayItem : ebayItems) {
                    query += "'" + ebayItem.getItemID() + "',";
                }
                query = query.substring(0, query.length() - 1) + "))";
                QueryResult salesforceQuery = salesforce.query(query);
                items.addAll((List<SObject>) (List<?>) Arrays.asList(salesforceQuery.getRecords()));
            }
        }

        return items;
    }

    /**
     * Attaches an existing buyer account or creates one if not existing to a salesforce item when an ebay buyer pays for the item.
     * @param sfItem The salesforce item.
     * @param ebayItem The ebayItem
     * @param orderType The ebay order.
     * @param transaction The ebay transaction.
     * @return The salesforce item with the attached buyer.
     * @throws ConnectionException
     */
    public SObject findOrCreateBuyerAndAttach(SObject sfItem, ItemType ebayItem, OrderType orderType, TransactionType transaction) throws ConnectionException {
        if (ebayItem.getSellingStatus().getHighBidder() == null) {
            return sfItem;
        }
        List<SObject> buyers = (List<SObject>) (List<?>) Arrays.asList(salesforce.query("select Id, FirstName, LastName, eBay_UserName__c, PersonEmail from Account where eBay_UserName__c = '" + ebayItem.getSellingStatus().getHighBidder().getUserID() + "'").getRecords());
        SObject personAccount = new SObject();
        personAccount.setType("RecordType");
        personAccount.setField("Name", "Person Account");
        SObject buyer = new SObject();
        buyer.setType("Account");
        buyer.setField("RecordType", personAccount);
        if(buyers.size() > 0) {
            buyer = buyers.get(0);
            buyer.removeField("Id");
        }
        String[] buyerName = orderType.getShippingAddress().getName().split(" ");
        String firstName, lastName;

        firstName = buyerName[0];
        if (buyerName.length == 2 && buyerName[1].length() > 0) {
            lastName = buyerName[1];
        } else if (buyerName.length == 3 && buyerName[2].length() > 0) {
            lastName = buyerName[2];
        } else {
            lastName = "Unknown";
        }

        if (!ebayItem.getSellingStatus().getHighBidder().getEmail().equalsIgnoreCase("Invalid Request")) {
            buyer.setField("PersonEmail", ebayItem.getSellingStatus().getHighBidder().getEmail());
        }
        buyer.setField("eBay_UserName__c", ebayItem.getSellingStatus().getHighBidder().getUserID());

        buyer.setField("FirstName", firstName);
        buyer.setField("LastName", lastName);

        buyer.setField("ShippingStreet", orderType.getShippingAddress().getStreet1() + " " + orderType.getShippingAddress().getStreet2());
        buyer.setField("ShippingCity", orderType.getShippingAddress().getCityName());
        buyer.setField("ShippingState", orderType.getShippingAddress().getStateOrProvince());
        buyer.setField("ShippingPostalCode", orderType.getShippingAddress().getPostalCode());
        if (orderType.getShippingAddress().getCountry() != null) {
            buyer.setField("ShippingCountry", orderType.getShippingAddress().getCountry().value());
        }
        buyer.setField("Phone", orderType.getShippingAddress().getPhone());
        buyer.setField("Buyer__c", true);
        UpsertResult[] results = salesforce.upsert("Id", new SObject[]{buyer});
        if (results[0].getErrors().length == 0) {
            sfItem.setField("Buyer__c", results[0].getId());
        } else {
            String exception = "";
            for (Error error : results[0].getErrors()) {
                exception += error.getMessage() + "<br/>";
            }
            throw new ConnectionException(exception);
        }
        return sfItem;
    }

    /**
     * Create a salesforce order from an ebay order.
     * @param sfItem Salesforce item.
     * @param orderType Ebay order.
     * @return A salesforce item with the order attached.
     * @throws ConnectionException
     */
    public SObject findOrCreateOrder(SObject sfItem, OrderType orderType) throws ConnectionException {
        QueryResult salesforceQuery = salesforce.query("Select id from Order__c where Ebay_Transaction_Num__c = '" + orderType.getOrderID() + "'");
        List<SObject> orders = (List<SObject>) (List<?>) Arrays.asList(salesforceQuery.getRecords());
        SObject order = new SObject();
        order.setType("Order__c");

        if (orders.size() > 0) {
            order = orders.get(0);
            order.removeField("Id");
        }
        if (!orderType.getShippingAddress().getStreet1().isEmpty() && orderType.getShippingAddress().getStreet2() != null) {
            order.setField("Shipping_Street__c", orderType.getShippingAddress().getStreet1() + " " + orderType.getShippingAddress().getStreet2());
        }
        if (!orderType.getShippingAddress().getCityName().isEmpty()) {
            order.setField("Shipping_City__c", orderType.getShippingAddress().getCityName());
        }
        if (!orderType.getShippingAddress().getStateOrProvince().isEmpty()) {
            order.setField("Shipping_State__c", orderType.getShippingAddress().getStateOrProvince());
        }
        if (!orderType.getShippingAddress().getPostalCode().isEmpty()) {
            order.setField("Shipping_Zip__c", orderType.getShippingAddress().getPostalCode());
        }
        if (orderType.getShippingAddress().getCountry()!= null &&
                orderType.getShippingAddress().getCountry().value().equals("USA")) {
            order.setField("Shipping_Country__c", "US");
        } else if (orderType.getShippingAddress().getCountry() != null){
            order.setField("Shipping_Country__c", orderType.getShippingAddress().getCountry().value());
        }
        order.setField("Order_Type__c", "eBay/Marketplaces");
        order.setField("Account__c", sfItem.getField("Buyer__c"));
        order.setField("Shipping_Service__c", orderType.getShippingServiceSelected().getShippingService());
        order.setField("Shipping_Comment__c", orderType.getBuyerCheckoutMessage());
        if (orderType.getShippingServiceSelected().getShippingServiceCost() != null) {
            order.setField("Shipping_Cost__c", orderType.getShippingServiceSelected().getShippingServiceCost().getValue());
        } else {
            order.setField("Shipping_Cost__c", 0.00);
        }
        order.setField("Shipping_Name__c", orderType.getShippingAddress().getName());
        order.setField("Sales_Tax_Amount__c", orderType.getShippingDetails().getSalesTax().getSalesTaxAmount().getValue());
        order.setField("Discount__c", 0.00);
        order.setField("Discount_Amount__c", 0.00);
        order.setField("Payment_Type_1__c", "PayPal");
        order.setField("Payment_Type_2__c", "PayPal");
        order.setField("Payment_Type_3__c", "PayPal");
        order.setField("Payment_Amount_1__c", orderType.getAmountPaid().getValue());
        order.setField("Payment_Amount_2__c", 0.00);
        order.setField("Payment_Amount_3__c", 0.00);
        order.setField("Selling_Channel__c", "MktPlace: eBay");
        order.setField("Ebay_Transaction_Num__c", orderType.getOrderID());

        UpsertResult[] results = salesforce.upsert("Id", new SObject[]{order});
        if (results[0].getErrors().length == 0) {
            sfItem.setField("Order__c", results[0].getId());
            sfItem.setField("Shipping_Comment__c", orderType.getBuyerCheckoutMessage());
        } else {
            String exception = "";
            for (Error error : results[0].getErrors()) {
                exception += error.getMessage() + "<br/>";
            }
            exception += " order information: "+order.toString();
            throw new ConnectionException(exception);
        }
        return sfItem;
    }
}
