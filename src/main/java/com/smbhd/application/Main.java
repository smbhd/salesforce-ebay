package com.smbhd.application;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Initializes the spring applicationContext
 */
public class Main {

    /**
     * Initializes the spring applicationContext
     * @param args Not used.
     */
    public static void main(String[] args) {
        // Initialize the application context and get the application running.
        new ClassPathXmlApplicationContext("applicationContext.xml");
    }
}
