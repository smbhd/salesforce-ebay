package com.smbhd.application;

import org.springframework.stereotype.Component;

import java.io.StringWriter;
import java.io.PrintWriter;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.Properties;

@Component
public class MailUtil {
    private static final String SMTP_Host = "smtp.gmail.com";
    private static final String SMTP_User = "smbhdlgs@gmail.com";
    private static final String SMTP_Pass = "a1234567!";
    private static final String toUser = "salesforce@smbhd.com";
    private static final String toAidan = "aidan@luxurygaragesale.com";
    private static final String toDom = "dom@luxurygaragesale.com";

    private static Session mailSess;

    public MailUtil() {
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.host", SMTP_Host);
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.auth", "true");


        Authenticator auth = new SMTPAuthenticator();
        mailSess = Session.getDefaultInstance(props, auth);
    }

    public void send(Exception excptnBody) {
        try {
            Transport tpt = mailSess.getTransport();
            MimeMessage message = new MimeMessage(mailSess);
            Multipart multipart = new MimeMultipart("alternative");

            BodyPart bPart = new MimeBodyPart();
            bPart.setContent(getStringFromException(excptnBody), "text/html");
            multipart.addBodyPart(bPart);

            message.setContent(multipart);
            message.setFrom(new InternetAddress(SMTP_User));
            message.setSubject("LGS Salesforce Connection Exception");
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toUser));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toAidan));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(toDom));

            tpt.connect();
            tpt.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
            tpt.close();
        }
        catch (Exception e) {
            System.out.println("Email Error: "+e.getMessage());
        }
    }

    private String getStringFromException(Exception e) {
        StringWriter errors = new StringWriter();
        e.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

    private static class SMTPAuthenticator extends javax.mail.Authenticator {
        public PasswordAuthentication getPasswordAuthentication() {
            String username = SMTP_User;
            String password = SMTP_Pass;
            return new PasswordAuthentication(username, password);
        }
    }
}
