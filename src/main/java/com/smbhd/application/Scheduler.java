package com.smbhd.application;

import com.ebay.soap.eBLBaseComponents.*;
import com.google.common.collect.Iterators;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.bind.XmlObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class Scheduler {

    @Autowired
    private SalesforceService salesforce;
    @Autowired
    private EbayService ebay;
    @Autowired
    private MailUtil mailUtil;

    @Scheduled(cron = "0 */2 * * * ?")
    public void listOnEbay() {
        List<SObject> sfItems = new ArrayList<>();
        try {
            sfItems = salesforce.getItemsToList();
        } catch(ConnectionException sfException) {
            System.out.println("ERROR: Failed retrieving items to list from Salesforce! " + sfException);
            mailUtil.send(sfException);
        }
        sfItems = listItemsOnEbay(sfItems); // SF -> EB
        updateInBatches(sfItems);

        //Listing done, clear sf items
        sfItems = new ArrayList<>();

        try {
            sfItems = salesforce.getItemsForEbay();
        } catch(ConnectionException sfException) {
            System.out.println("ERROR: Failed retrieving ebay-ready items from Salesforce! " + sfException);
            mailUtil.send(sfException);
        }
        sfItems = relistItems(sfItems); // SF -> EB
        sfItems = reviseItems(sfItems); // SF -> EB
        sfItems = endEbayItems(sfItems); // SF -> EB
        sfItems = trackingItems(sfItems); // SF -> EB

        updateInBatches(sfItems);

        pollEbayInfo(); // SF <- EB
        pollEbayOrders(); // SF <- EB
        getMessages(); // SF <- EB
    }

    private void updateInBatches(List<SObject> sfItems) {
        // We can only update 200 at a time, otherwise the whole update fails.
        List<SObject> sfItemsToUpdate = new ArrayList<>();
        for (SObject sfItem : sfItems) {
            sfItemsToUpdate.add(sfItem);
            if (sfItemsToUpdate.size() == 200) {
                try {
                    salesforce.updateItems(sfItemsToUpdate);
                    sfItemsToUpdate.clear();
                } catch(ConnectionException sfException) {
                    mailUtil.send(sfException);
                    sfItemsToUpdate.clear();
                }
            }
        }
        if(sfItemsToUpdate.size() > 0) {
            try {
                salesforce.updateItems(sfItemsToUpdate);
            } catch (ConnectionException sfException) {
                mailUtil.send(sfException);
            }
        }
    }

    /**
     * Retrieves a list of new salesforce items, then lists them on Ebay.
     */
    private List<SObject> listItemsOnEbay(List<SObject> sfItems) {
        for (SObject sfItem : sfItems) {
            if(sfItem.getField("Item_Status__c").equals("Scheduled for Ebay") && !Boolean.valueOf(sfItem.getField("EndListingMarked__c").toString()) && !Boolean.valueOf(sfItem.getField("RelistListingMarked__c").toString()) && !Boolean.valueOf(sfItem.getField("ReviseListingMarked__c").toString())) {
                try {
                    String recordTypeDevName = salesforce.getRecordTypeDevName((String)sfItem.getField("RecordTypeId"));
                    ItemType ebayItem = ebay.buildEbayItem(sfItem, recordTypeDevName);
                    List<SObject> attachments = salesforce.getItemAttachments(sfItem);
                    if (attachments.size() > 0) {
                        attachments.sort((SObject a1, SObject a2) -> a1.getField("Name").toString().compareTo(a2.getField("Name").toString()));
                        ebayItem = ebay.attachPictures(ebayItem, attachments);
                    }
                    ebayItem.setDescription(salesforce.getTemplateHTML(sfItem, ebayItem));
                    Calendar endTime = ebay.listItem(ebayItem);
                    if (endTime != null) {
                        sfItem.setField("Ebay_Listing_End_Date__c", endTime);
                        sfItem.setField("Ebay_Listing_Number__c", ebayItem.getItemID());
                        sfItem.setField("eBay_Views__c", 0.0);
                        sfItem.setField("Bids__c", 0.0);
                        sfItem.setField("Image_Count__c", Double.valueOf(attachments.size()));
                        if (!sfItem.getField("Item_Status__c").toString().equalsIgnoreCase("Listed on eBay/Web")) {
                            if (!Boolean.valueOf(sfItem.getField("Shopify__c").toString())) {
                                sfItem.setField("Item_Status__c", "Listed on eBay Only");
                            }
                            else {
                                sfItem.setField("Item_Status__c", "Listed on eBay, Pending Web Listing");
                            }
                        }
                        sfItem.setField("Ebay__c", true); 
                    }
                } catch (ConnectionException sfException) {
                    sfItem.setField("Applicaiton_Error_Time__c", Calendar.getInstance());
                    sfItem.setField("Application_Error_Message__c", "[SF:listItemsOnEbay] " + sfException.getMessage());
                    mailUtil.send(sfException);

                } catch (Exception ebayException) {
                    if (ebayException.getMessage() != null && ebayException.getMessage().contains("This Listing is a duplicate of your item:")) {
                        correctDuplication(sfItem, ebayException.getMessage());
                    } else {
                        sfItem.setField("Applicaiton_Error_Time__c", Calendar.getInstance());
                        sfItem.setField("Application_Error_Message__c", "[EB:listItemsOnEbay] " + ebayException.getMessage());
                        ebayException.printStackTrace(System.out);
                    }
                }
            }
        }
        return sfItems;
    }

    /**
     * Polls ebay for information on items such as bids or buy status and updates them on salesforce.
     */
    private void pollEbayInfo() {
        Map<String, ItemType> ebayItemsByListingNum = new HashMap<>();
        List<SObject> sfItems = new ArrayList<>();
        try {
            List<ItemType> ebayItems = ebay.getSellerEvents();
            for (ItemType ebayItem : ebayItems) {
                ebayItemsByListingNum.put(ebayItem.getItemID(), ebayItem);
            }
        } catch (Exception ebayException) {
            System.out.println("ERROR: Failed getting seller events from ebay! " + ebayException);
            mailUtil.send(ebayException);
        }
        if (ebayItemsByListingNum.isEmpty()) {
            return;
        }
        try {
            List<ItemType> ebayItems = new ArrayList<>();
            ebayItems.addAll(ebayItemsByListingNum.values());
            sfItems = salesforce.getItemsToUpdate(ebayItems);
        } catch(ConnectionException sfException) {
            mailUtil.send(sfException);
        }
        for (SObject sfItem : sfItems) {
            ItemType ebayItem = ebayItemsByListingNum.get(sfItem.getField("Ebay_Listing_Number__c"));
            if (ebayItem == null) { // Item not purchased on ebay, skip
                continue;
            }
            try {
                if (ebayItem.getSellingStatus().getListingStatus().equals(ListingStatusCodeType.COMPLETED)) {
                    if (ebayItem.getSellingStatus().getQuantitySold() < 1) {
                        sfItem = setItemEnded(sfItem);
                    }
                } else if (ebayItem.getSellingStatus().getListingStatus().equals(ListingStatusCodeType.ENDED)) {
                    if (ebayItem.getSellingStatus().getQuantitySold() < 1) {
                        sfItem = setItemEnded(sfItem);
                    }
                } else {//still active
                    if (!sfItem.getField("Item_Status__c").toString().equalsIgnoreCase("Listed on eBay/Web") && !sfItem.getField("Item_Status__c").toString().equalsIgnoreCase("Out on Lux Box")) {
                        if (!Boolean.valueOf(sfItem.getField("Shopify__c").toString())) {
                            sfItem.setField("Item_Status__c", "Listed on eBay Only");
                        }
                        else {
                            sfItem.setField("Item_Status__c", "Listed on eBay, Pending Web Listing");
                        }
                    }
                    if (ebayItem.getHitCount() != null) {
                        sfItem.setField("eBay_Views__c", ebayItem.getHitCount().doubleValue());
                    }
                    if (ebayItem.getWatchCount() != null) {
                        sfItem.setField("eBay_Watch_Count__c", ebayItem.getWatchCount().doubleValue());
                    }
                    if (!ebayItem.getListingType().value().equals(ListingTypeCodeType.FIXED_PRICE_ITEM.value())) {
                        sfItem.setField("Bids__c", ebayItem.getSellingStatus().getBidCount().doubleValue());
                    }
                }
            } catch (Exception ebayException) {
                sfItem.setField("Applicaiton_Error_Time__c", Calendar.getInstance());
                sfItem.setField("Application_Error_Message__c", "[EB:pollEbayInfo] " + ebayException.getMessage());
                ebayException.printStackTrace(System.out);
            }
        }
        updateInBatches(sfItems);
    }

    /**
     * Polls ebay for ebay orders that have happened within the last day.
     */
    private void pollEbayOrders() {
        // Get Orders
        List<OrderType> orders = new ArrayList<>();
        try {
            orders = ebay.getOrders();
        } catch (Exception ebayException) {
            System.out.println("ERROR: Failed getting orders from ebay! " + ebayException);
            mailUtil.send(ebayException);
        }
        // Get list of ebayItems to get items from salesforce to update
        Map<String, ItemType> ebayItemsByListingNumber = new HashMap<>();
        for (OrderType order : orders) {
            List<TransactionType> transactions = Arrays.asList(order.getTransactionArray().getTransaction());
            for (TransactionType transaction : transactions) {
                if (transaction != null && transaction.getItem() != null) {
                    try {
                        ItemType ebayItem = ebay.getItem(transaction.getItem().getItemID());
                        ebayItemsByListingNumber.put(ebayItem.getItemID(), ebayItem);
                    } catch (Exception ebayException) {
                        System.out.println("ERROR: Failed getting item from ebay! " + ebayException);
                        mailUtil.send(ebayException);
                    }
                }
            }
        }
        if (ebayItemsByListingNumber.isEmpty()) {
            return;
        }
        ArrayList<SObject> sfItems = new ArrayList<>();
        try {
            ArrayList<ItemType> ebayItems = new ArrayList<>();
            ebayItems.addAll(ebayItemsByListingNumber.values());
            sfItems = salesforce.getItemsToUpdate(ebayItems);
        } catch(ConnectionException sfException) {
            mailUtil.send(sfException);
        }
        // Map sfItems to ebay listing
        Map<String, SObject> sfItemByEbayListing = new HashMap<>();
        for (SObject sfItem : sfItems) {
            sfItemByEbayListing.put((String)sfItem.getField("Ebay_Listing_Number__c"), sfItem);
        }
        for (OrderType order : orders) {
            List<TransactionType> transactions = Arrays.asList(order.getTransactionArray().getTransaction());
            for (TransactionType transaction : transactions) {
                if (transaction != null && transaction.getItem() != null) {
                    ItemType ebayItem = ebayItemsByListingNumber.get(transaction.getItem().getItemID());
                    if (ebayItem == null) {
                        continue;
                    }
                    SObject sfItem = sfItemByEbayListing.get(ebayItem.getItemID());
                    if (sfItem == null) { // Item not in listed status, so must have been bought offline or manually transitioned.
                        continue;
                    }
                    try {
                        sfItem = salesforce.findOrCreateBuyerAndAttach(sfItem, ebayItem, order, transaction);
                        sfItem.setField("Purchased_Through__c", "MktPlace: eBay");
                        sfItem.setField("Ebay_Listing_End_Date__c", ebayItem.getListingDetails().getEndTime());
                        sfItem.setField("Sold_Date__c", order.getCreatedTime());
                        sfItem.setField("Best_Offer_Sale__c", transaction.isBestOfferSale());
                        if (ebayItem.getHitCount() != null) {
                            sfItem.setField("eBay_Views__c", ebayItem.getHitCount().doubleValue());
                        }
                        if (ebayItem.getWatchCount() != null) {
                            sfItem.setField("eBay_Watch_Count__c", ebayItem.getWatchCount().doubleValue());
                        }
                        if (!ebayItem.getListingType().value().equals(ListingTypeCodeType.FIXED_PRICE_ITEM.value())) {
                            sfItem.setField("Bids__c", ebayItem.getSellingStatus().getBidCount().doubleValue());
                        }
                        if (order.getAmountPaid() != null && order.getAmountPaid().getValue() > 0 && order.getPaidTime() != null) {//paid
                            sfItem.setField("Item_Status__c", "Sold/Paid");
                            sfItem.setField("Current_Price__c", transaction.getTransactionPrice().getValue());
                            sfItem.setField("Paid_Date__c", order.getPaidTime());
                            sfItem.setField("Customer_Paid_Amount__c", transaction.getTransactionPrice().getValue());
                            sfItem.setField("Bids__c", ebayItem.getSellingStatus().getBidCount().doubleValue());
                            sfItem.setField("Shipping_Service__c", order.getShippingServiceSelected().getShippingService());
                            sfItem.setField("Buyer_Message__c", transaction.getBuyerCheckoutMessage());
                            sfItem = salesforce.findOrCreateOrder(sfItem, order);
                        } else {
                            sfItem.setField("Item_Status__c", "Sold/Unpaid");
                            sfItem.setField("Best_Offer_Sale__c", transaction.isBestOfferSale());
                            sfItem.setField("Current_Price__c", transaction.getTransactionPrice().getValue());
                            sfItem = salesforce.findOrCreateOrder(sfItem, order);
                        }
                        sfItemByEbayListing.put((String) sfItem.getField("Ebay_Listing_Number__c"), sfItem);
                    } catch (ConnectionException sfException) {
                        sfItem.setField("Applicaiton_Error_Time__c", Calendar.getInstance());
                        sfItem.setField("Application_Error_Message__c", "[SF:pollEbayOrders] " + sfException.getMessage());
                        mailUtil.send(sfException);
                    } catch (Exception ebayException) {
                        sfItem.setField("Applicaiton_Error_Time__c", Calendar.getInstance());
                        sfItem.setField("Application_Error_Message__c", "[EB:pollEbayOrders] " + ebayException.getMessage());
                        ebayException.printStackTrace(System.out);
                    }
                }
            }
        }
        sfItems.clear();
        sfItems.addAll(sfItemByEbayListing.values());
        updateInBatches(sfItems);
    }


    /**
     * Gets items in salesforce that need a manual relisting. Goes to ebay and ends item if active, relists it.
     */
    private List<SObject> relistItems(List<SObject> sfItems) {
        for (SObject sfItem : sfItems) {
            if(!Boolean.valueOf(sfItem.getField("RelistListingMarked__c").toString())) {
                continue;
            }
            try {
                ItemType ebayItem = ebay.getItem((String)sfItem.getField("Ebay_Listing_Number__c"));
                if (ebayItem == null) { // Item not purchased on ebay, skip
                    sfItem.setField("RelistListingMarked__c", false); // It was edited in salesforce and marked relisted when it should have been scheduled only.
                    continue;
                }
                if (ebayItem.getSellingStatus().getListingStatus().equals(ListingStatusCodeType.ACTIVE)) {
                    ebay.endItem((String)sfItem.getField("Ebay_Listing_Number__c"));
                }
                String recordTypeDevName = salesforce.getRecordTypeDevName((String)sfItem.getField("RecordTypeId"));
                ebayItem = ebay.setItemAttributes(sfItem, ebayItem, recordTypeDevName);
                List<SObject> attachments = salesforce.getItemAttachments(sfItem);
                if (attachments.size() > 0) {
                    attachments.sort((SObject a1, SObject a2) -> a1.getField("Name").toString().compareTo(a2.getField("Name").toString()));
                    ebayItem = ebay.attachPictures(ebayItem, attachments);
                }
                ebayItem.setDescription(salesforce.getTemplateHTML(sfItem, ebayItem));
                Calendar endTime = ebay.relistItem(ebayItem);
                if (endTime != null) {
                    sfItem.setField("Ebay_Listing_End_Date__c", endTime);
                    sfItem.setField("Ebay_Listing_Number__c", ebayItem.getItemID());
                    sfItem.setField("Current_Price__c", sfItem.getField("Starting_Price__c"));
                    sfItem.setField("eBay_Views__c", 0.0);
                    sfItem.setField("Bids__c", 0.0);
                    if (!sfItem.getField("Item_Status__c").toString().equalsIgnoreCase("Listed on eBay/Web")) {
                        if (!Boolean.valueOf(sfItem.getField("Shopify__c").toString())) {
                            sfItem.setField("Item_Status__c", "Listed on eBay Only");
                        }
                        else {
                            sfItem.setField("Item_Status__c", "Listed on eBay, Pending Web Listing");
                        }
                    }
                    sfItem.setField("Ebay__c", true);
                    sfItem.setField("RelistListingMarked__c", false);
                }
            } catch(ConnectionException sfException) {
                sfItem.setField("Applicaiton_Error_Time__c", Calendar.getInstance());
                sfItem.setField("Application_Error_Message__c", "[SF:relistItems] " + sfException.getMessage());
                mailUtil.send(sfException);
            } catch (Exception ebayException) {
                if (ebayException.getMessage() != null && ebayException.getMessage().contains("This item cannot be accessed because the listing has been deleted, is a Half.com listing, or you are not the seller.")) {
                    sfItem.setField("Ebay_Listing_Number__c", " ");
                    sfItem.setField("Item_Status__c", "Scheduled for Ebay");
                } else if (ebayException.getMessage() != null && ebayException.getMessage().contains("This Listing is a duplicate of your item:")) {
                    correctDuplication(sfItem, ebayException.getMessage());
                } else {
                    sfItem.setField("Applicaiton_Error_Time__c", Calendar.getInstance());
                    sfItem.setField("Application_Error_Message__c", "[EB:relistItems] " + ebayException.getMessage());
                    ebayException.printStackTrace(System.out);
                }
            }
        }
        return sfItems;
    }

    /**.
     * Gets items in salesforce that need to end. Ends item on ebay and reflects it in salesforce.
     */
    private List<SObject> endEbayItems(List<SObject> sfItems) {
        for (SObject sfItem : sfItems) {
            if(!Boolean.valueOf(sfItem.getField("EndListingMarked__c").toString())) {
                continue;
            }
            try {
                ItemType ebayItem = ebay.getItem((String)sfItem.getField("Ebay_Listing_Number__c"));
                if (ebayItem != null && ListingStatusCodeType.ACTIVE.equals(ebayItem.getSellingStatus().getListingStatus())) {
                    Calendar endTime = ebay.endItem((String)sfItem.getField("Ebay_Listing_Number__c"));
                    if (!sfItem.getField("Item_Status__c").toString().equals("Repairs/Cleaners") &&
                            !sfItem.getField("Item_Status__c").toString().equals("Discarded") &&
                            !sfItem.getField("Item_Status__c").toString().equals("In Review") &&
                            !sfItem.getField("Item_Status__c").toString().equals("Pending Consignor Return") &&
                            !sfItem.getField("Item_Status__c").toString().equals("Returned To Consignor") &&
                            !sfItem.getField("Item_Status__c").toString().equals("Donated") &&
                            !sfItem.getField("Item_Status__c").toString().equals("Sold/Paid") &&
                            !sfItem.getField("Item_Status__c").toString().equals("Sold/Unpaid") &&
                            !sfItem.getField("Item_Status__c").toString().equals("Shipped") &&
                            !sfItem.getField("Item_Status__c").toString().equals("Sold/Paid - Pending Local Pick-Up")) {
                        sfItem.setField("Item_Status__c", "Unsold");
                    }
                    sfItem.setField("Ebay_Listing_End_Date__c", endTime);
                    sfItem.setField("EndListingMarked__c", false);
                    sfItem.setField("Ebay__c", false);
                } else {
                    sfItem.setField("EndListingMarked__c", false);
                }
            } catch(ConnectionException sfException) {
                sfItem.setField("Applicaiton_Error_Time__c", Calendar.getInstance());
                sfItem.setField("Application_Error_Message__c", "[SF:endEbayItems] " + sfException.getMessage());
                mailUtil.send(sfException);
            } catch (Exception ebayException) {
                if (ebayException.getMessage().contains("This item cannot be accessed because the listing has been deleted, is a Half.com listing, or you are not the seller.")) {
                    sfItem.setField("Ebay_Listing_Number__c", " ");
                } else {
                    sfItem.setField("Applicaiton_Error_Time__c", Calendar.getInstance());
                    sfItem.setField("Application_Error_Message__c", "[EB:endEbayItems] " + ebayException.getMessage());
                    ebayException.printStackTrace(System.out);
                }
            }
        }
        return sfItems;
    }

    /**
     * Get items in salesforce that need to get revised. Update their counterparts in ebay.
     */
    private List<SObject> reviseItems(List<SObject> sfItems) {
        for (SObject sfItem : sfItems) {
            if(!Boolean.valueOf(sfItem.getField("ReviseListingMarked__c").toString())) {
                continue;
            }
            try {
                ItemType ebayItem = ebay.getItem((String)sfItem.getField("Ebay_Listing_Number__c"));
                if (ebayItem != null && ebayItem.getSellingStatus().getListingStatus().equals(ListingStatusCodeType.ACTIVE)) {
                    String recordTypeDevName = salesforce.getRecordTypeDevName((String)sfItem.getField("RecordTypeId"));
                    ebayItem = ebay.setItemAttributes(sfItem, ebayItem, recordTypeDevName);
                    List<SObject> attachments = salesforce.getItemAttachments(sfItem);
                    if (attachments.size() > 0) {
                        attachments.sort((SObject a1, SObject a2) -> a1.getField("Name").toString().compareTo(a2.getField("Name").toString()));
                        ebayItem = ebay.attachPictures(ebayItem, attachments);
                    }
                    ebayItem.setDescription(salesforce.getTemplateHTML(sfItem, ebayItem));
                    ebay.reviseItem(ebayItem);
                    if (!sfItem.getField("Item_Status__c").toString().equalsIgnoreCase("Listed on eBay/Web")) {
                        if (!Boolean.valueOf(sfItem.getField("Shopify__c").toString())) {
                            sfItem.setField("Item_Status__c", "Listed on eBay Only");
                        }
                        else {
                            sfItem.setField("Item_Status__c", "Listed on eBay, Pending Web Listing");
                        }
                    }
                }
                sfItem.setField("ReviseListingMarked__c", false);
            } catch(ConnectionException sfException) {
                sfItem.setField("Applicaiton_Error_Time__c", Calendar.getInstance());
                sfItem.setField("Application_Error_Message__c", "[SF:reviseItems] " + sfException.getMessage());
                mailUtil.send(sfException);
            } catch (Exception ebayException) {
                sfItem.setField("Applicaiton_Error_Time__c", Calendar.getInstance());
                sfItem.setField("Application_Error_Message__c", "[EB:reviseItems] " + ebayException.getMessage());
            }
        }
        return sfItems;
    }

    /**
     * Set item ended
     */
    private SObject setItemEnded(SObject sfItem) {
        if (sfItem.getField("Ebay_Listing_Type__c").toString().equals("Auction") && Boolean.valueOf(sfItem.getField("Auto_Relist_on_Unsold__c").toString()) && Double.valueOf(sfItem.getField("Relist_Counter__c").toString()) < Double.valueOf(sfItem.getField("Auto_Relist_Limit__c").toString())) {
            sfItem.setField("Item_Status__c", "Scheduled for Ebay");
            sfItem.setField("RelistListingMarked__c", true);
            sfItem.setField("Relist_Counter__c", Double.valueOf(sfItem.getField("Relist_Counter__c").toString()) + 1);
        } else {
            sfItem.setField("Item_Status__c", "Unsold");
            sfItem.setField("EndListingMarked__c", false);
        }
        return sfItem;
    }

    /**
     * Get messages from ebay
     */
    private void getMessages() {
        try {
            List<MyMessagesMessageType> ebayMessages = ebay.getMessages();
            // Once we have all of the messages from Ebay, send them to salesforce.
            List<SObject> sfMessages = new ArrayList<>();
            for (MyMessagesMessageType message : ebayMessages) {
                SObject sfMessage = new SObject();
                sfMessage.setType("Message__c");
                sfMessage.setField("From__c", message.getSender());
                sfMessage.setField("Date__c", message.getReceiveDate());
                sfMessage.setField("EBay_Listing__c", message.getItemID());
                sfMessage.setField("Subject__c", message.getSubject());
                sfMessage.setField("Body__c", message.getText());
                sfMessage.setField("Ebay_Message_Id__c", message.getMessageID());
                sfMessage.setField("Selling_Channel__c", "eBay");
                if (message.getMessageType() != null) {
                    sfMessage.setField("Message_Type__c", message.getMessageType().value());
                }

                /*
                Question type is not yet implemented in Ebay, manually setting it for now
                if (message.getQuestionType() != null) {
                    sfMessage.setQuestion_Type__c", message.getQuestionType().value());
                }
                */
                String questionType = "";
                String subject = message.getSubject().toLowerCase();
                if (subject.contains("received an offer") || (subject.contains("offer received") && !subject.contains("counter"))) {
                    questionType = "New Offer";
                }
                else if (subject.contains("counteroffer for") || subject.contains("counter offer received")) {
                    questionType = "Buyer Counteroffer";
                }
                else if (subject.contains("counteroffer submitted")) {
                    questionType = "LGS Counteroffer";
                }
                else if (subject.contains("offer expired")) {
                    questionType = "Expired Offer";
                }
                sfMessage.setField("Question_Type__c", questionType);

                sfMessage.setField("Replied__c", message.isReplied());
                sfMessages.add(sfMessage);
            }
            salesforce.upsertMessages(sfMessages);
        } catch (ConnectionException sfException) {
            mailUtil.send(sfException);
        } catch (Exception ebayException) {
            System.out.println("ERROR: Failed to get messages from ebay! " + ebayException);
            mailUtil.send(ebayException);
        }
    }

    /**
     * Send tracking information to ebay
     */
    private List<SObject> trackingItems(List<SObject> sfItems) {
        for (SObject sfItem : sfItems) {
            if(!Boolean.valueOf(sfItem.getField("Tracking_Sent_To_Ebay__c").toString()) && sfItem.getField("Shipment__c") != null && sfItem.getField("Ebay_Listing_Number__c") != null
                    && ((XmlObject)sfItem.getField("Shipment__r")).getField("Tracking_Number__c") != null && ((XmlObject)sfItem.getField("Shipment__r")).getField("Shipping_Type__c") != null) {
                try {
                    ebay.sendTrackingInfo(sfItem);
                    sfItem.setField("Tracking_Sent_To_Ebay__c", true);
                } catch (ConnectionException sfException) {
                    sfItem.setField("Applicaiton_Error_Time__c", Calendar.getInstance());
                    sfItem.setField("Application_Error_Message__c", "[SF:trackingItems] " + sfException.getMessage());
                    mailUtil.send(sfException);
                } catch (Exception ebayException) {
                    sfItem.setField("Applicaiton_Error_Time__c", Calendar.getInstance());
                    sfItem.setField("Application_Error_Message__c", "[EB:trackingItems] " + ebayException.getMessage());
                    ebayException.printStackTrace(System.out);
                }
            }
        }
        return sfItems;
    }

    /**
     * Correct duplication
     */
    private SObject correctDuplication(SObject sfItem, String message) {
        try { // Duplicate logic correction.
            ItemType ebayItem = ebay.getItem(message.substring(message.indexOf('(') + 1, message.indexOf('(') + 13));
            sfItem.setField("Application_Error_Message__c", message);
            sfItem.setField("Ebay_Listing_End_Date__c", ebayItem.getListingDetails().getEndTime());
            sfItem.setField("Ebay_Listing_Number__c", ebayItem.getItemID());
            sfItem.setField("Current_Price__c", ebayItem.getSellingStatus().getCurrentPrice().getValue());
            sfItem.setField("eBay_Views__c", 0.0);
            sfItem.setField("Bids__c", 0.0);
            sfItem.setField("Image_Count__c", Double.valueOf(Iterators.size(sfItem.getChildren("Attachments"))));
            sfItem.setField("RelistListingMarked__c", false);
            if (!sfItem.getField("Item_Status__c").toString().equalsIgnoreCase("Listed on eBay/Web")) {
                if (!Boolean.valueOf(sfItem.getField("Shopify__c").toString())) {
                    sfItem.setField("Item_Status__c", "Listed on eBay Only");
                }
                else {
                    sfItem.setField("Item_Status__c", "Listed on eBay, Pending Web Listing");
                }
            }
            sfItem.setField("Ebay__c", true);
        } catch (Exception ebayException2) {
            sfItem.setField("Applicaiton_Error_Time__c", Calendar.getInstance());
            sfItem.setField("Application_Error_Message__c", "[EB:correctDuplication] " + ebayException2.getMessage());
            ebayException2.printStackTrace(System.out);
        }
        return sfItem;
    }
}
